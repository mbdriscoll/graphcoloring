#ifndef _CS267_GRAPHGEN_H_
#define _CS267_GRAPHGEN_H_

#include <upc.h>

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

void init_rmat_graph( int seed, int nVerts, int nEdges,
    /* out */ shared int ** rowPtrs,
    /* out */ shared int ** colInds);

#endif /* _CS267_GRAPHGEN_H_ */
