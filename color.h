#ifndef _CS267_COLOR_H_
#define _CS267_COLOR_H_

#include <upc.h>
#include <upc_collective.h>

void color(int nVerts,
    shared int *rowPtrs,
    shared int *colInds,
    shared int *colors);

#endif /* _CS267_COLOR_H_ */
