CC = gcc-mp-4.7
UPCC = upcc
UPCC_FLAGS = -network=smp -pthreads -DGASNETT_USE_GCC_ATTRIBUTE_MAYALIAS

OBJS = driver.o viz.o graphgen.o util.o color.o timer.o

default: color

color: $(OBJS)
	$(UPCC) $(UPCC_FLAGS) -o $@ $^

%.o: %.upc
	$(UPCC) $(UPCC_FLAGS) -o $@ -c $^

.PHONY: clean

clean:
	rm -rf color $(OBJS) *_pthread-link *.dot
