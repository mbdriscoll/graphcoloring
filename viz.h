#ifndef _CS267_VIZ_H
#define _CS267_VIZ_H_

#include <stdio.h>
#include <upc.h>

/* Maximum number of vertices allowed when
 * visualizing results. */
#define MAX_VIZ_VERTS 128

void visualize(int nVerts, int nColors,
    shared int *rowPtrs, shared int *colInds, shared int *colors,
    char *outfilename);

#endif /* _CS267_VIZ_H_ */
