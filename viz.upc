#include "viz.h"

void write_dot(int nVerts, int nColors,
    shared int *rowPtrs, shared int *colInds, shared int *colors,
    FILE *outfile) {

    // header
    fprintf(outfile, "graph {\n");
    fprintf(outfile, "  rankdir=LR;\n");

    // nodes
    for (int i = 0; i < nVerts; i++) {
        float hue = colors[i] / (float) nColors;
        fprintf(outfile,
            "  %d [style=filled,shape=circle,fillcolor=\"%f 1.0 1.0\"];\n",
            i, hue);
    }

    // edges
    for (int i = 0; i < nVerts; i++) {
        for (int idx = rowPtrs[i]; idx < rowPtrs[i+1]; idx++) {
            int j = colInds[idx];
            if (i < j) // avoid printing edges twice
                fprintf(outfile, "  %d -- %d;\n", i, j);
        }
    }

    // footer
    fprintf(outfile, "}\n");
}

void visualize(int nVerts, int nColors,
    shared int *rowPtrs, shared int *colInds, shared int *colors,
    char *outfilename) {

    if (MYTHREAD == 0) {
        if (nVerts > MAX_VIZ_VERTS) {
            printf("WARNING: Too many vertices to visualize (%d > %d).\n", nVerts, MAX_VIZ_VERTS);
        } else {
            FILE *outfile = fopen(outfilename, "w");
            if (outfile == NULL) {
                printf("ERROR: Cannot open file: %s.\n", outfilename);
                exit(1);
            }
            write_dot(nVerts, nColors, rowPtrs, colInds, colors, outfile);
            fclose(outfile);
            printf("INFO: Graph written to %s\n", outfilename);
        }
    }
}

