#include <stdio.h>
#include <stdlib.h>

#include "color.h"

void color(int nVerts,
    shared int *rowPtrs, shared int *colInds, shared int *colors) {

    upc_forall(int i = 0; i < nVerts; i++; &colors[i])
        colors[i] = i % 5;
}
