#include <alloca.h>
#include <float.h>

#include <upc_collective.h>

#include "graphgen.h"

int max_degree(int nElems, shared int *degrees) {
    shared int * maxDegreeV = upc_all_alloc(1, sizeof(int));
    upc_all_reduceI(maxDegreeV, degrees, UPC_MAX, nElems, 1, NULL, UPC_IN_ALLSYNC | UPC_OUT_ALLSYNC);
    int maxDegree = maxDegreeV[0];
    upc_all_free(maxDegreeV);
    return maxDegree;
}

int max_degree_rowPtrs(int nVerts, shared int *rowPtrs) {
    shared int *degrees = upc_all_alloc(nVerts, sizeof(int));
    upc_forall(int i = 0; i < nVerts; i++; &degrees[i])
        degrees[i] = rowPtrs[i+1] - rowPtrs[i];
    int maxDegree = max_degree(nVerts, degrees);
    upc_all_free(degrees);
    return maxDegree;
}

int compare_int(const void* a, const void* b) {
    return *(int*)a - *(int*)b;
}

int count_multiedges(int nVerts, shared int *rowPtrs, shared int *colInds) {

    int maxDegree = max_degree_rowPtrs(nVerts, rowPtrs);
    int *edges = (int*) alloca(maxDegree*sizeof(int));

    shared int *multiedgesv = upc_all_alloc(THREADS, sizeof(int));
    shared int *multiedges  = upc_all_alloc(1, sizeof(int));
    multiedgesv[MYTHREAD] = 0;

    // check every vertex
    upc_forall(int i = 0; i < nVerts; i++; &rowPtrs[i]) {
        int idx = rowPtrs[i];
        int nEdges = rowPtrs[i+1] - idx;

        // copy edges locally
        for (int e = 0; e < nEdges; e++)
            edges[e] = colInds[idx+e];

        // compare all edges
        for (int x = 0; x < nEdges; x++)
            for (int y = x+1; y < nEdges; y++)
                if (edges[x] == edges[y])
                    multiedgesv[MYTHREAD] += 1;
    }

    upc_all_reduceI(multiedges, multiedgesv, UPC_ADD, THREADS, 1, NULL, UPC_IN_ALLSYNC | UPC_OUT_ALLSYNC);

    return multiedges[0];
}
void desymmetrize_graph( int nVerts,
    shared int *  sym_rowPtrs,
    shared int *  sym_colInds,
    shared int ** _rowPtrs,
    shared int ** _colInds ) {

    int nEdges = sym_rowPtrs[nVerts];
    shared int * degrees = upc_all_alloc(nVerts+1, sizeof(int));
    shared int * offset  = upc_all_alloc(nVerts+1, sizeof(int));
    shared int * rowPtrs = upc_all_alloc(nVerts+1, sizeof(int));
    shared int * colInds = upc_all_alloc(nEdges*2, sizeof(int));

    upc_forall(int i = 0; i < nVerts+1; i++; &degrees[i])
        degrees[i] = offset[i] = 0;

    upc_barrier; // =========================================================

    // increment remote degrees based on outgoing edges
    upc_forall(int i = 0; i < nVerts; i++; &sym_rowPtrs[i]) {
        for (int idx = sym_rowPtrs[i]; idx < sym_rowPtrs[i+1]; idx++) {
            int j = sym_colInds[idx];
            bupc_atomicI_fetchadd_relaxed(&degrees[i+1], 1); // offset by one to simplify prefix scan
            bupc_atomicI_fetchadd_relaxed(&degrees[j+1], 1);
        }
    }

    // compute new rowPtrs
    upc_all_prefix_reduceI(rowPtrs, degrees, UPC_ADD, nVerts+1, 1, NULL, UPC_IN_ALLSYNC | UPC_OUT_ALLSYNC);

    // copy edges into new colInds
    upc_forall(int i = 0; i < nVerts; i++; &sym_rowPtrs[i]) {
        for (int sym_idx = sym_rowPtrs[i]; sym_idx < sym_rowPtrs[i+1]; sym_idx++) {
            int j = sym_colInds[sym_idx];
            int i_idx = rowPtrs[i] + bupc_atomicI_fetchadd_relaxed(&offset[i], 1);
            int j_idx = rowPtrs[j] + bupc_atomicI_fetchadd_relaxed(&offset[j], 1);
            colInds[i_idx] = j;
            colInds[j_idx] = i;
        }
    }

    upc_barrier; // =========================================================

    upc_all_free(degrees);
    upc_all_free(offset);

    *_rowPtrs = rowPtrs;
    *_colInds = colInds;
}

void sort(int nVals, int *vals) {
    int i, j;
    for (i = 1; i < nVals; i++) {
        int value = vals[i];
        for (j = i; j > 0 && value < vals[j-1]; j--)
            vals[j] = vals[j - 1];
        vals[j] = value;
    }
}

int expNumDrawsForXUniqueVals(int n /*populationsize*/, int x /*unique*/) {
    float k = 0.0f;
    for (int i = 1; i <= x; i++)
        k += n / (n - i + 1.0);
    return (int) k + 0.5;
}

void rmat_sample(int nVerts, float a, float b, float c, float d, int *_x, int *_y) {

    int x = 0,
        y = 0,
        width = nVerts;

    for (int width = nVerts; width > 1; width >>= 1) {
        float i = rand() / (RAND_MAX + 1.0f);

        if      (i < a)                   { /* nothing */ }
        else if (i < a+b)                 { x += width/2; }
        else if (i < a+b+c)               { y += width/2; }
        else if (i < a+b+c+d+FLT_EPSILON) { x += width/2;
                                            y += width/2; }
        else if (i > a+b+c+d+FLT_EPSILON) {
            printf("WARN: rmat probabilities aren't normalized (they sum to %f, i was %f).\n", a+b+c+d, i);
            exit(3);
        }
    }

    *_x = x;
    *_y = y;
}

void init_rmat_graph( int seed, int nVerts, int nEdges,
    /* out */ shared int ** rowPtrs,
    /* out */ shared int ** colInds ) {

    float a = 0.25f, b = 0.25f, c = 0.25f, d = 0.25f;

    shared int *degrees = upc_all_alloc(nVerts+1, sizeof(int));
    shared int *added   = upc_all_alloc(nVerts+1, sizeof(int));

    upc_forall(int i = 0; i < nVerts+1; i++; &degrees[i])
        added[i] = degrees[i] = 0;

    upc_barrier; //==========================================================

    // Throw darts at the adjacency matrix to determine edges.
    // Trials are independent and might lead to collisions, so run enough
    // that we get the expected number of unique elements.
    int nMatrixEntries = nVerts * (nVerts-1) / 2;
    // account for collisions plus rejected samples
    int nTrials = expNumDrawsForXUniqueVals(nMatrixEntries, nEdges) * (2.0 * nVerts / (nVerts - 1.0f));
    int nTrialsPerThread = (nTrials+THREADS-1) / THREADS;
    int *src_edges = (int*) alloca(nTrialsPerThread*sizeof(int)),
        *dst_edges = (int*) alloca(nTrialsPerThread*sizeof(int));

    int nLocalTrials = 0;
    for (int i = MYTHREAD; i < nTrials; i += THREADS, nLocalTrials++) {
        srand((seed+i+1338)*5303892480);
        int x, y;
        do {
            rmat_sample(nVerts, a, b, c, d, &x, &y);
        } while (x <= y);

        bupc_atomicI_fetchadd_relaxed(&degrees[x+1], 1); // offset by one to simplify prefix scan

        src_edges[nLocalTrials] = x;
        dst_edges[nLocalTrials] = y;
    }

    // allocate vector for temp rowPtrs
    shared int * tmp_rowPtrs = upc_all_alloc(nVerts+1, sizeof(int));

    // build the row ptr vector
    upc_all_prefix_reduceI(tmp_rowPtrs, degrees, UPC_ADD, nVerts+1, 1, NULL, UPC_IN_ALLSYNC | UPC_OUT_ALLSYNC);

    // allocate temp colInds
    int nTmpEdges = tmp_rowPtrs[nVerts];
    shared int * tmp_colInds = upc_all_alloc(nTmpEdges, sizeof(int));

    // fill in edges
    for (int i = 0; i < nLocalTrials; i++) {
        int x = src_edges[i],
            y = dst_edges[i];

        int base = tmp_rowPtrs[x];
        int offset = bupc_atomicI_fetchadd_relaxed(&added[x], 1);
        tmp_colInds[base+offset] = y;
    }

    // remove duplicates: compute new degrees reflecting unique edges
    shared int *tmp_degrees = upc_all_alloc(nVerts+1, sizeof(int));
    upc_forall(int i = 0; i < nVerts+1; i++; &tmp_degrees[i])
        tmp_degrees[0] = 0;

    upc_barrier; // =========================================================

    upc_forall(int r = 0; r < nVerts; r++; &tmp_rowPtrs[r]) {
        int degree = tmp_rowPtrs[r+1] - tmp_rowPtrs[r];
        int *edges = (int*) alloca(degree*sizeof(int));

        // copy edges locally
        int base = tmp_rowPtrs[r];
        for (int j = 0; j < degree; j++)
            edges[j] = tmp_colInds[base+j];

        // sort edges
        qsort(edges, degree, sizeof(int), compare_int);

        // remove duplicates
        int src, dst;
        for (src = 0, dst = 0; src < degree; src++) {
            if (src == 0 || edges[src] != edges[dst-1])
                edges[dst++] = edges[src];
        }

        // copy edges back to shared array
        base = tmp_rowPtrs[r];
        for (int j = 0; j < dst; j++)
            tmp_colInds[base+j] = edges[j];

        // set new degree
        tmp_degrees[r+1] = dst;
    }

    // build the new row ptr vector
    shared int *sym_rowPtrs = upc_all_alloc(nVerts+1, sizeof(int));
    upc_all_prefix_reduceI(sym_rowPtrs, tmp_degrees, UPC_ADD, nVerts+1, 1, NULL, UPC_IN_ALLSYNC | UPC_OUT_ALLSYNC);

    // allocate space for new edges
    int nSymEdges = sym_rowPtrs[nVerts];
    shared int *sym_colInds = upc_all_alloc(nSymEdges, sizeof(int));

    // copy unique edges
    upc_forall(int i = 0; i < nVerts; i++; &sym_rowPtrs[i]) {
        int degree = sym_rowPtrs[i+1] - sym_rowPtrs[i];
        int sym_base = sym_rowPtrs[i],
            tmp_base = tmp_rowPtrs[i];
        for (int offset = 0; offset < degree; offset++)
            sym_colInds[ sym_base+offset ] = tmp_colInds[ tmp_base+offset ];
    }

    upc_barrier; // =========================================================

    // de-symmetrize graph to build full adjaceny list
    desymmetrize_graph(nVerts, sym_rowPtrs, sym_colInds, rowPtrs, colInds);

    upc_barrier; // =========================================================

    // cleanup
    upc_all_free(added);
    upc_all_free(degrees);
    upc_all_free(sym_rowPtrs);
    upc_all_free(sym_colInds);
    upc_all_free(tmp_degrees);
    upc_all_free(tmp_rowPtrs);
    upc_all_free(tmp_colInds);

    upc_barrier; // =========================================================

    // check that the graph we made isn't a multi-graph
    int nMultiEdges = count_multiedges(nVerts, *rowPtrs, *colInds);
    if (MYTHREAD == 0 && nMultiEdges != 0)
        printf("WARNING: Found %d multi-edges\n", nMultiEdges);
    assert(nMultiEdges == 0);

    // wait for every to intialize
    upc_barrier; // =========================================================

    if (MYTHREAD == 0) {
        int nEdges = (*rowPtrs)[nVerts];
        int bytes = (nVerts + nVerts+1 + nEdges) * sizeof(int);
        printf("INFO: Graph has %d vertices, %d edges, takes %d MB.\n",
                nVerts, (*rowPtrs)[nVerts] / 2, bytes / 1024 / 1024);
    }
}

