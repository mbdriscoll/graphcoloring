#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <upc.h>
#include <upc_collective.h>

#include "timer.h"

int isPowerOfTwo(int x) {
    return ((x != 0) && !(x & (x - 1)));
}

void print_help(char *argv[]) {
    if (MYTHREAD == 0) {
        printf("Usage: %s [ options ]\n", argv[0]);
        printf("  where options include:\n");
        printf("    -o <file>   File to write with DOT representation of graph.\n");
        printf("    -n <N>      Set number of vertices in the graph to 2^N.\n");
        printf("    -e <N>      Maximum allowed error rate (0.0 to 1.0).\n");
        printf("    -m <N>      Set expected number of edges.\n");
        printf("    -i          Print a histogram of vertex degrees.\n");
        printf("    -h          Show help.\n");
    }

    exit(2);
}

int main(int argc, char *argv[]) {

    char *outfilename = NULL;
    int seed = 1868;
    int nVerts = 32;
    float maxErrorRate = 0.0f;
    int nEdges = 40;
    int showHelp = 0;
    int printHistogram = 0;

    for (int i = 1; i < argc; i++) {
        if      (!strcmp(argv[i], "-o"))
            outfilename = argv[++i];
        else if (!strcmp(argv[i], "-s"))
            seed = atoi(argv[++i]);
        else if (!strcmp(argv[i], "-n"))
            nVerts = (int) pow(2.0f, atof(argv[++i]));
        else if (!strcmp(argv[i], "-e"))
            maxErrorRate = atof(argv[++i]);
        else if (!strcmp(argv[i], "-m"))
            nEdges = atoi(argv[++i]);
        else if (!strcmp(argv[i], "-i"))
            printHistogram = 1;
        else if (!strcmp(argv[i], "-h"))
            showHelp = 1;
        else {
            if (MYTHREAD == 0)
                printf("ERROR: Unrecognized commandline parameter: %s\n", argv[i]);
            showHelp = 1;
        }
    }

    if (showHelp)
        print_help(argv);

    if (MYTHREAD == 0 && !isPowerOfTwo(nVerts))
        printf("WARNING: r-mat works best with powers-of-two number of vertices (not %d).\n", nVerts);

    // Graph representation
    shared int *rowPtrs;
    shared int *colInds;
    shared int *colors = upc_all_alloc(nVerts, sizeof(int));

    // Initialize the graph
    // TODO: other types of graph (scale-free especially)
    init_rmat_graph( seed, nVerts, nEdges, &rowPtrs, &colInds );

    upc_barrier;

    if (printHistogram == 1)
        print_degree_histogram(nVerts, rowPtrs);

    upc_barrier;

    // Color the vertices
    double start_time = wall_time();
    color( nVerts, rowPtrs, colInds, colors );
    upc_barrier;
    double duration = wall_time() - start_time;

    if (MYTHREAD == 0)
        printf("INFO: coloring took %f ms.\n", duration*1000.0);

    // Check the solution
    check(nVerts, rowPtrs, colInds, colors, maxErrorRate);

    // Score the solution
    int nColors = score(nVerts, colors);

    // Visualize the solution
    if (outfilename != NULL)
        visualize(nVerts, nColors, rowPtrs, colInds, colors, outfilename);

    return 0;
}
