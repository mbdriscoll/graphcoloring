#ifndef _CS267_UTIL_H_
#define _CS267_UTIL_H_

#include <upc.h>

void check(int nVerts, shared int *rowPtrs, shared int *colInds, shared int *colors, float maxErrorRate);

int score(int nVerts, shared int *colors);

#endif /* _CS267_UTIL_H_ */
