#include <upc_collective.h>

#include <util.h>

int score(int nVerts, shared int *colors) {
    shared int * nColorsv = upc_all_alloc( THREADS, sizeof(int) );
    upc_all_reduceI(nColorsv, colors, UPC_MAX, nVerts, 1, NULL, UPC_IN_ALLSYNC | UPC_OUT_ALLSYNC);

    int nColors = nColorsv[MYTHREAD]+1;
    if (MYTHREAD == 0)
        printf("INFO: Used %d colors.\n", nColors);
    return nColors;
}

void print_degree_histogram(int nVerts, shared int *rowPtrs) {
    shared int *degrees = upc_all_alloc(nVerts, sizeof(int));
    upc_forall(int i = 0; i < nVerts; i++; &rowPtrs[i])
        degrees[i] = rowPtrs[i+1] - rowPtrs[i];

    shared int *maxDegreeV = upc_all_alloc(THREADS, sizeof(int));
    bupc_all_reduce_allI(maxDegreeV, degrees, UPC_MAX, nVerts, 1, NULL, UPC_IN_ALLSYNC | UPC_OUT_ALLSYNC);

    int maxDegree = maxDegreeV[MYTHREAD];
    shared int *hist = upc_all_alloc(maxDegree, sizeof(int));
    upc_forall(int i = 0; i < maxDegree; i++; &hist[i])
        hist[i] = 0;

    if (MYTHREAD == 0)
        printf("INFO: max degree is %d\n", maxDegree);

    upc_barrier; // =========================================================

    upc_forall(int i = 0; i < nVerts; i++; &degrees[i])
        bupc_atomicI_fetchadd_relaxed(&hist[degrees[i]], 1);

    upc_barrier; // =========================================================

    if (MYTHREAD == 0) {
        printf("INFO: degree histogram -> ");
        for (int i = 0; i < maxDegree; i++) {
            printf("%d", hist[i]);
            if (i+1 != maxDegree)
                printf(", ");
            else
                printf("\n");
        }
    }

    upc_barrier; // =========================================================

    if (0 && MYTHREAD == 0) {
        upc_free(maxDegreeV);
        upc_free(degrees);
        upc_free(hist);
    }

    upc_barrier; // =========================================================
}

void check(int nVerts, shared int *rowPtrs, shared int *colInds, shared int *colors, float maxErrorRate) {

    upc_barrier;

    // vectors to hold error counts on each thread
    shared int * nErrorsPerThread = upc_all_alloc(THREADS, sizeof(int));
    shared int * nTotalErrors     = upc_all_alloc(THREADS, sizeof(int));
    nErrorsPerThread[MYTHREAD] = nTotalErrors[MYTHREAD] = 0;

    // count errors
    upc_forall(int i = 0; i < nVerts; i++; &rowPtrs[i]) {
        int this_color = colors[i];
        for(int idx = rowPtrs[i]; idx < rowPtrs[i+1]; idx++) {
            int j = colInds[idx];
            if (i < j && this_color == colors[j]) // don't double-count
                nErrorsPerThread[MYTHREAD] += 1;
        }
    }

    // sum errors
    upc_all_reduceI(nTotalErrors, nErrorsPerThread, UPC_ADD, THREADS, 1, NULL, UPC_IN_ALLSYNC | UPC_OUT_ALLSYNC);

    // report errors
    if (MYTHREAD == 0) {
        int nErrors = nTotalErrors[0];
        int nEdges = rowPtrs[nVerts] / 2;
        float errorRate = nErrors / (float) nEdges;

        if (errorRate > maxErrorRate)
            printf("ERROR: %2.2f%% (%d/%d) edges joined vertices of the same color.\n",
                    100.0f* nErrors/(float)nEdges, nErrors, nEdges);
        else
            printf("INFO: Success!\n");
    }

    upc_barrier;

    // cleanup
    if (MYTHREAD == 0) {
        upc_free(nErrorsPerThread);
        upc_free(nTotalErrors);
    }

    upc_barrier;
}

